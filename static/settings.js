var JL_SETTINGS = {
    // Debug berichten in console.log() weergeven? Kan hier aangepast worden
    debugging: true,
    // api url
    api: "http://localhost:8080",
    // image cache url
    imgcacheurl: "http://localhost:63343/web/static/avatars",
    // Current location file name
    currentLocation: window.location.pathname.substring(window.location.pathname.lastIndexOf("/") + 1),
    // Pagina waar de vragenlijst is
    questionsLocation: "index.html",
    // Pagina met vraag en zijn antwoorden
    answersLocation: "answers.html",
    // Pagina met berichten
    inboxLocation: "inbox.html",
    // Pagina met gebruikerlijst
    usersLocation: "users.html",
    // Login form
    loginLocation: "login.html",
    // E-mail verification page
    verifyLocation: "verify.html",
    // gebruikersid
    id: parseInt(Cookies.get("id"))
};

// Condities

// Welke vragen moeten geladen worden?
JL_SETTINGS.paramFilter = getUrlParam("filter") === null ? "new" : getUrlParam("filter");
// Welke berichten moeten geladen worden?
JL_SETTINGS.msgParamFilter = getUrlParam("filter") === null ? "inbox" : getUrlParam("filter");
// Pagination begint bij 0 in Spring
JL_SETTINGS.paramPage = getUrlParam("page") === null ? 0 : parseInt(getUrlParam("page"));
// Bij individuele (vraag, antwoord, ...) objecten, over welk id gaat het
JL_SETTINGS.paramId = parseInt(getUrlParam("id"));

// API debugging config
$.fn.api.settings.debug = JL_SETTINGS.debugging;
$.fn.api.settings.verbose = false;
$.fn.api.settings.performance = false;

$.fn.api.settings.base = JL_SETTINGS.api;
$.fn.api.settings.api = {
    'get me'              : "/users/me",
    'get tags'            : "/tags",
    'get top tags'        : "/tags?filter={filter}",
    'get question'        : "/questions/{id}",
    'get questions'       : "/questions?page={/page}&filter={/filter}",
    'get answers'         : "/questions/{id}/answers?page={/page}",
    'get image'           : "/users/{id}/image",
    'get users'           : "/users?page={/page}",
    'get user'            : "/users/{id}",
    'get message'         : "/messages/{id}",
    'get messages'        : "/messages?page={/page}&filter={/filter}",
    'get unread messages' : "/messages/unread",
    'create user'         : "/users",
    'create question'     : "/questions",
    'create answer'       : "/questions/{id}/answers",
    'create tag'          : "/tags",
    'create message'      : "/messages",
    'create reply'        : "/messages/reply",
    'edit user'           : "/users/{id}",
    'edit answer'         : "/answers/{id}",
    'edit question'       : "/questions/{id}",
    'edit image'          : "/users/{id}/image",
    'delete answer'       : "/answers/{id}",
    'delete question'     : "/questions/{id}",
    'delete image'        : "/users/{id}/image",
    'delete message'      : "/messages/{id}",
    'search questions'    : "/questions/search",
    'search by tag'       : "/questions/search?tag={tagid}&filter={/filter}",
    'mark answer'         : "/questions/{questionId}/answers/{id}",
    'like answer'         : "/questions/{questionId}/answers/{id}",
    'mark msg read'       : "/messages/{id}",
    'verify email'        : "/users/emailverification/{id}?seed={seed}"
};

// HTTP basic auth for API calls
if (Cookies.get("auth") !== undefined) {
    $.fn.api.settings.beforeXHR = function(xhr){
        xhr.setRequestHeader("Authorization", "Basic " + Cookies.get("auth"));
    }
    // If cookie is unset, redirect to login form
} else if (JL_SETTINGS.currentLocation !== JL_SETTINGS.loginLocation
    && JL_SETTINGS.currentLocation !== JL_SETTINGS.verifyLocation) {
    location.href = JL_SETTINGS.loginLocation + "?mustlogin";
}

/*$.fn.api.settings.onFailure = function(response){
    debug("Problem with JSON response in XHR request: " + response);
};*/

$.fn.api.settings.contentType = "application/json";

// Empty strings become nulls
if ($.serializeJSON !== undefined) {
    $.serializeJSON.defaultOptions.skipFalsyValuesForTypes = ["string"];
}

//=====
// Functions
//=====

function getUrlParam(name) {
    var url = new URL(window.location.href),
        p = url.searchParams.get(name);

    debug("getUrlParam(): Value for URL param '" + name + "' is " + p);
    return p;
}

function debug(message) {
    if (JL_SETTINGS.debugging) {
        console.log(message);
    }
}

function startLoadingDotsAnimation(elm) {
    var elmId = elm instanceof jQuery ? elm.find('.loadingdots') : $(elm + ' .loadingdots');

    debug("startLoadingDotsAnimation: starting animation");

    setInterval(function(){
        if (elmId.text().length === 3) {
            elmId.text("");
        }
        else {
            elmId.text(elmId.text() + ".");
        }
    }, 300);
}

function showFormMessage(selector, allowMultiple) {
    var selectorElm = $(selector);
        allowMultiple = allowMultiple || false;

    if (!allowMultiple) {
        selectorElm.parent('form').find(".message").transition("hide");
    }

    if (selectorElm.is(':hidden')) {
        selectorElm.transition("drop");
    }
}

function isFormInputChanged(form) {
    var $elements = form.find('input,textarea,select'),
        returnValue = false;

    $elements.each(function() {
        var newValue,
            oldValue = $(this).data("oldvalue");

        if ($(this).is(":checkbox")) {
            newValue = $(this).prop("checked");
        } else if ($(this).is("select")) {
            newValue = $(this).val().toString();
        } else {
            newValue = $(this).val().trim();
        }

        if ((newValue !== "" && newValue !== oldValue) || (newValue === "" && (oldValue !== undefined && oldValue !== ""))) {
            returnValue = true;
            // As soon as a change was detected, we don't need to continue the loop
            return false;
        }
    });
    return returnValue;
}

/**
 * Toon een bericht met ladende puntjes en stuur dan door naar een locatie na 3 seconden
 *
 * @param selector Selector van element die getoond moet worden en waarin zich een .loadingdots bevindt
 * @param page Locatie waarnaar door te sturen
 * @param callback Optionele callback functie
 */
function showLoadingAnimationAndRedirect(selector, page, callback) {
    // optioneel
    callback = callback || null;

    showFormMessage(selector);
    startLoadingDotsAnimation(selector);
    setTimeout(function(){
        if (callback !== null) {
            callback();
        }
        location.href = page;
    }, 3000);
}

/**
 * Formateer een naam om te tonen, kan enkel een voornaam zijn of combinatie van voornaam en achternaam als
 * deze laatste ook gegeven is.
 * @param object Question of Answer object
 */
function getNameOutOfObject(object) {
    var firstName, lastName;

    if (object['user'] !== undefined) {
        firstName = object['user']['firstName'];
        lastName = object['user']['lastName'] === null ? "" : " " + object['user']['lastName'];
    }
    else {
        firstName = object['firstName'];
        lastName = object['lastName'] === null ? "" : " " + object['lastName'];
    }
    firstName = firstName.charAt().toUpperCase() + firstName.slice(1);
    lastName = lastName.charAt(0).toUpperCase() + lastName.slice(1);

    return stripHTML(firstName + lastName);
}

/**
 * Verantwoordelijk voor het maken en beheren van de pagination component.
 * Heeft een Page (vraag, antwoord, ...) response object nodig om de juiste data uit te halen.
 * Zoekt naar #paginationsegm en past de pagination element daar toe
 */
function addPagination(response) {
    var currentURL = new URI(window.location.href),
        itemTemplate = "<a class=\"item\"></a>",
        pagesegmElm = $('#paginationsegm'),
        leftArrowId = pagesegmElm.find('a:first'),
        rightArrowId = pagesegmElm.find('a:last');

    if (response['totalPages'] === 0 || response['totalPages'] === 1) {
        debug("There is only one page, pagination skipped");
        pagesegmElm.hide();
        return;
    }

    debug("Current page is " + response['number']);
    debug("Total pages: " + response['totalPages']);

    for (var n = 0; n < response['totalPages']; n++) {
        var item = $(itemTemplate);

        if (response['number'] === n) {
            item.addClass("active");
        }
        item.prop("href", currentURL.search(function(d){d.page = n}));
        // Visueel is 1 aangenamer dan 0
        rightArrowId.before(item.text(n + 1));
    }
    leftArrowId.prop("href", currentURL.search(function(d){d.page = (response['number'] - 1)}));
    rightArrowId.prop("href", currentURL.search(function(d){d.page = (response['number'] + 1)}));

    // Minimum pagina, deactiveer linkse pijl
    if (response['first']) {
        leftArrowId.addClass("disabled").removeAttr("href");
    }
    // Maximum pagina, deactiveer rechtse pijl
    if (response['last']) {
        rightArrowId.addClass("disabled").removeAttr("href");
    }
    pagesegmElm.show();
}

/**
 * Error template
 */
function errorMessage(msg) {
    return "<div class=\"ui pointing red label\">" + msg + "</div>";
}

function showSuccessToast(message, callback) {
    callback = callback || null;

    $('body').toast({
        class: "success",
        showProgress: "bottom",
        progressUp: false,
        message: message + " <i class='grin beam icon'></i>",
        onRemove: callback
    });
}

function showFailToast(message, callback) {
    callback = callback || null;

    $('body').toast({
        class: "error",
        showProgress: "bottom",
        progressUp: false,
        message: message + " <i class='sad tear icon'></i>",
        onRemove: callback
    });
}

function jsonParseEmptyAsNull(val, inputName) {
    return val === "" ? null : val;
}

/**
 * Logout event handler
 */
$('#logoutBtn').on("click", function() {
    removeAuthCookies();

    $('body').prepend(
        "<div id=\"logoutmodal\" class=\"ui basic modal\">" +
        "<span class=\"ui large text\">Je wordt nu uitgelogd<span class=\"loadingdots\">...</span></span>\n" +
        "</div>");

    var logoutmodalElm = $('#logoutmodal');

    startLoadingDotsAnimation(logoutmodalElm);
    logoutmodalElm.modal('show');

    setTimeout(function() {
        location.href = JL_SETTINGS.loginLocation;
    }, 2000)
});

function removeAuthCookies() {
    Cookies.remove("auth");
    Cookies.remove("id");
}

var unreadCount = 0;

/**
 * Ping for new messages
 */
function xhrPingUnreadMessages() {
    var $inboxMenuItem = $('.menu:first').find('.mail').parent();

    $inboxMenuItem.api({
        action: "get unread messages",
        on: "now",
        onSuccess: function(r) {
            var $inboxLabel = $inboxMenuItem.find('.label');

            if (r['count'] > 0 && unreadCount !== r['count']) {
                unreadCount = r['count'];

                if (!$inboxLabel.length) {
                    $inboxMenuItem.html($inboxMenuItem.html() + "<div class=\"ui red left pointing hidden label\">" + r['count'] + "</div>");
                    $inboxMenuItem.find('.hidden').transition("zoom");
                } else {
                    $inboxLabel.text(r['count']);
                    $inboxLabel.transition("jiggle");
                }
            }
        }
    });

    setInterval(function() {
        $inboxMenuItem.api("query");
    }, 10000);
}

/**
 * Convert HTML to text
 */
function stripHTML(dirtyString) {
    var container = document.createElement('div'),
        text = document.createTextNode(dirtyString);

    container.appendChild(text);
    return container.innerHTML; // innerHTML will be a xss safe string
}